2008-05-30, Berlin

This picture shows Ian and me at the LinuxTag 2008 in Berlin. I was volunteering
at the Debian booth and wasn't expecting Ian to be there as well. When he
suddenly arrived I was surprised and very happy to meet the guy who started the
Debian project in person. He couldn't stay very long, but we had a little chat
and took the picture.

Almost 8 years later I don't really remember what we talked about but I do
remember that Ian made a very good impression on me. He seemed very calm and
friendly, and not even a tiny bit arrogant, although he initialized probably one
of the most important Linux distributions to date.

Farewell Ian and thank you for your contributions to society!


License of the Picture: Public Domain
