Photos and videos from the old LinEx Development Team

Attribution-NonCommercial-ShareAlike
https://creativecommons.org/licenses/by-nc-sa/2.0/

The photos are mainly from the meeting we had in Mérida, Extremadura in 2004,
two years after the LinEx distribution was born.

Some members of the Free Software Community as Ian, Till Kamppeter or Álvaro del Castillo joined
with José L. Redrejo, Antonio Ullán and other LinEx supporters to improve collaboration paths and
do an agreement with Progeny, the company Ian ruled.


